# WTF am I even running
One of the most impactful ways to understand a system is to debug it.
There are 5 branches of this repo on gitlab that follow the naming pattern 
`sabotage-*`. In each of them some evil entity deleted a line that should
break the development environment. A useful challenge is now to try to run
the environment from one of those branches and try to fix it.