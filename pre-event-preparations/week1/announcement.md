Hello dear participants!

As was announced in the GTKEO meeting, this week we start with the preparation for the event. We would appreciate if you all took about 3-4 hours a week to engage with what we prepared to bring you all on a good foundational level to start the event.

In the first week, we will focus on setting up your computer to easily work with the technologies we want to introduce you to.
In [week1/knowledge.md](knowledge.md) of the event's repository you'll find the two goals of the week and -as the name suggests- the corresponding knowledge.
We instructors are available to you on discord to help with all questions you might be having, don't be shy 🤭

In the following weeks, Seid and Umutcan will introduce you to some topics that will help you get into frontend and backend development with react/Laravel, so please stay tuned for that!

All the best,

Alex