Hello dear participants

We hope you're ready for the second week of our preparations for the upcoming IT Sprint. This week, we'll be focusing on Backend (👻scary, I know)

We have some exciting tasks and tutorials lined up for you, so make sure you're ready to polish your backend skills. All the tasks and details for the week can be found at the following GitLab link: [week2/knowledge.md](knowledge.md).

Don't forget to ask for help if you need it. I will be online during the week to jump in and help out.

Let's make this a successful week and bring our A-game to the backend section of the IT Sprint!

Best of luck,
Seid

P.S. if you need some music while learning, [here](https://www.youtube.com/watch?v=fCt2_AsCWKI&ab_channel=NFQAsia) you go, no need to thank me
