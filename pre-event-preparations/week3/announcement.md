# Week 3 begins

Hello everyone,

this is the last week.

During this week we will dive a bit deeper in the mystical forests of the frontend.

Our goal for this week is for you to get your hands wet with creating a small website. Things we will be covering are:

- A small refresher/intro to HTML
- A dive into CSS
- Mixing it all together with JS.

Don't forget to ping @umutcan or @Morpheus if you need help.

Best of luck,
Umutcan